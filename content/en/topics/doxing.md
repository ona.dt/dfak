---
layout: page
title: "I have been doxxed or someone is sharing my private info or media without my consent"
author: Ahmad Gharbeia, Michael Carbone, Gus Andrews, Alex Argüelles, (+constanza, +flo - since a & m built workflow from your drafts)
language: en
summary: "Private information about me or media featuring my likeness is being circulated. It is distressing to me or could potentially be harmful"
date: 2023-04
permalink: /en/topics/doxxed-or-info-nonconsensually-shared
parent: Home
---

# Someone is sharing my private information or media without my consent (I have been doxxed)

Harassers may sometimes publish information that personally identifies you or exposes aspects of your personal life that you have chosen to keep private. This is sometimes called "doxxing" (a shorthand term for "releasing documents").

The goal of doxxing is to embarrass the target or agitate against them. This can have severe negative effects on your psychosocial well-being, personal safety, relationships, and work.

Doxxing is used to intimidate public figures, journalists, human rights defenders, and activists, but also frequently targets people belonging to sexual minorities or marginalized populations. It is closely related to other forms of online and gendered violence.

The person doxxing you may have obtained your documents from various sources. This could include breaking into your private accounts and devices. But it is also possible that they are exposing and taking out of context information or media you thought you were sharing privately with your friends, partners, acquaintances, or co-workers. In some cases harassers may aggregate publicly-available information about you from social media or public records. Putting this information together in new, harmfuly misconstrued ways could constitute doxxing too.

If you have been doxxed, you can use the following questionnaire to identify where the harasser has found your information, and find ways to limit the damage, including removing the documents from websites.

## Workflow

### be_supported

> Documentation is fundamental for assessing the impacts, identifying the origin of these attacks, and developing responses that restore your safety. Depending on what happened, going through the documentation process could enhance the emotional impacts this aggression has on you. To lessen the burden of this work and reduce potential emotional stress on yourself, you may need support from a close friend or other trusted person while you document these attacks.

 - [Okay, a trusted friend is ready to help me if I need it](#second_question)

### physical_risk

> Reflect: Who do you think may be responsible for these attacks? What are their capabilities? How determined are they to harm you? What other parties may be motivated to do you harm? These questions will help you assess threats that are ongoing.

Do you feel at physical risk for your safety?

 - [Yes](#yes_physical_risk)
 - [No](#legal_risk)
 - [I am not sure. I need help assessing my risks.](#assessment_end)

### yes_physical_risk

> Recommendation
>
> [links & resources re: physical safety]

See the resources above to help with thinking through your immediate physical safety needs.

Do you feel you may be at legal risk as well?

 - [Yes](#legal_risk)
 - [No](#takedown_content)
 - [I am not sure. I need help assessing my risks.](#assessment_end)

### legal_risk

> Recommendation
>
> [Links & resources re: legal risk]

Please see the resources above to help with thinking through your immediate legal safety needs.

Do you want to work on getting the inappropriately-shared documents removed from public view?

 - [Yes](#documenting)
 - [No](#resolve_end)

### takedown_content

Do you want to work on getting the inappropriately-shared documents removed from public view?

 - [Yes](#documenting)
 - [No](#resolve_end)

### documenting
> Recommendation

Keeping your own documentation of the doxxing you were subjected to is very important. It makes it easier for legal or technical helpers to support you. link to DOCUMENTATION WORKFLOW. You may want to ask your trusted friend to help you look at the documents from the doxxing attack. This can help ensure you are not re-traumatized by looking at the doxxed materials again.

- [Okay, I am documenting the attacks](#where_published)

### where_published

> Depending on where the attacker has publicly shared your information, you may want to take different actions.

> If the doxxers posted your information on social media platforms based in countries with legal systems which define corporations' legal responsibilities towards people who use their services (for example, the European Union or the United States), it is more likely that the corporation's moderators will be willing to help you. They will need you to [document](#documenting) evidence of the attacks. You may be able to look up the country where a company is based on Wikipedia.

> Sometimes doxxers may publish your information on smaller, less-well-known sites, and link to them from social media. Sometimes these sites are hosted in countries where there is no legal protection, or they are run by individuals or groups who practice, endorse, or simply do not object to harmful behavior. In this case it may be more difficult to contact the sites where the material is posted, or find out who runs them.

> But even when the doxxer has posted your documents somewhere other than popular social media (which is often the case), working to take down those materials or links to them can greatly reduce how many people see your information. It can also reduce the number and severity of attacks you may face.

My information was posted on

- [a regulated platform or service under law](#what_info_published)
- [a platform or service with no likelihood of responding to takedown requests](#legal_advocacy_end)

### what_info_published

What information has been nonconsensually shared?

- [**personal information that identifies me or is private**: address, phone number, SSN, bank account, etc.](#personal_info)
- [**media I did not consent to share**, including intimate material (pictures, video, audio, texts)](#media)
- [**a pseudonym I use has been connected with my real-life identity**](#linked_identities)

### linked_identities
> If an aggressor publicly links your real-life name or identity to a pseudonym, nickname, or handle that you use when you express yourself, make your opinion public, organize, or engage in activism, consider closing accounts related to that identity as a way of reducing the damage.

Do you need to continue using this identity/persona to achieve your goals, or can you close the account(s)?

- [I need to keep using it](#defamation_flow_end)
- [I can close account(s)](#close)

### close
> Before closing the accounts associated with the affected identity or persona, consider the risks of closing the account. You might lose access to services or data, face risks to your reputation, lose contact with your online associates, etc.

- [I closed relevant accounts](#resolved_end)
- [I decided not to close the relevant accounts right now](#resolved_end)
- [I need help understanding what I should do](#help_persona_end)

### personal_info
> platform-specific content takedown requests, including webhosts and search engines LINKS.

Please find the resources above for reporting to different platforms.

Has the content been removed?

- [Yes](#resolved_end)
- [No, the platform did not respond](#platform_help_end)
- [No, the platform said it would not remove the content](#platform_help_end)
- [I did not find the relevant platforms in the resource list](#platform_help_end)

### media

Do you own the copyrights to the media? If you have created the media yourself, you usually do. In some jurisdictions, individuals also have rights to the media they appear in (known as rights of publicity).

Do you own the copyrights to the media?

- [Yes](#intellectual_property)
- [No](#nude)

### intellectual_property
> Many platforms and services have specific pages describing their policies to deal with copyright requests. Intellectual property laws (such as the DMCA) support your right to ask platforms to take your copyrighted information down.  
>
> Here are a few places where you can look for platforms' copyright policies:
> [resource list of common platforms]

Has the content been removed?

- [Yes](#resolved_end)
- [No, the platform did not respond](#platform_help_end)
- [No, the platform said it would not remove the content](#platform_help_end)
- [I did not find the relevant platforms in the resource list](#platform_help_end)

### nude
> Media that show nude bodies are sometimes covered by specific platform policies.

Does the media include nude bodies or details of nude bodies?

- [Yes](#intimate_media)
- [No](#nonconsensual_media)

### intimate_media
> You can tell platforms about violation of their rules about privacy, or the nonconsensual sharing of intimate media/harmful content, at these links:
> + LINKS

Has the content been removed?

- [Yes](#resolved_end)
- [No, the platform did not respond](#platform_help_end)
- [No, the platform said it would not remove the content](#platform_help_end)
- [I did not find the relevant platforms in the resource list](#platform_help_end)

### nonconsensual_media
> report non-consensual publication of private media
>

Has the content been removed?

- [Yes](#resolved_end)
- [No, the platform did not respond](#platform_help_end)
- [No, the platform said it would not remove the content](#platform_help_end)
- [I did not find the relevant platforms in the resource list](#platform_help_end)

### assessment_end

> If you feel that you need help in assessing the risks you face because your documents have been shared without your permission, please contact the organizations below who can support you.


:[](organisations?services=harassment&services=assessment)

### resolved_end

> You have done heroic work to protect yourself from the digital violence you faced. Well done.
> Now you can take some time to breathe and be mindful of your well-being.

> Here are some [additional tips](final_tips) that you might find useful.

### legal_advocacy_end

> In some cases, the platforms where the doxxing is taking place do not have policies or a moderation process that are mature enough to handle requests regarding doxxing, harassment or defamation.

> Sometimes the offensive material is published or hosted on platforms or apps that are run by individuals or groups that practice, endorse, or simply do not object to harmful behaviour.

> There also places in the cyberspace where regular laws and regulations are hardly enforceable, because they are specifically created to operate anonymously and leave no trace. One of these is what is known as *the dark web*. There are benefits to making anonymous spaces like these available, even though some people abuse them.

> In situations like these when legal action is not available, consider whether you are willing to resort to advocacy. This could include publicly shedding light on your case, generating a public debate about it, and possibly surfacing similar cases. There are rights organisations and groups that can help you with that. They can help you set expectations for outcomes, and give you a sense of the ramifications.

> Seeking legal aid may be a last resort if communication with the platform, app, or service provider fails or is impossible.

> The legal path usually takes time, costs money, and may require that you publicize that doxxing happened to you. A successful legal case depends on many factors, including [documentation](#documenting) of the aggression in a way that is deemed acceptable by courts, and the legal framework that governs the case. Identifying the perpetrators or determining that they were responsible may also be difficult to prove. It may also be challenging to establish the jurisdiction in which the case should be tried.


### defamation_flow_end

### platform_help_end
> Below is a list of organizations who can support you in reporting to platforms and services.
:[](organisations?services=triage)

### final_tips

> If the doxxing involved information or documents that only you, or a trusted few people, should have known or had access to, then you might also want to reflect on how the doxxer accessed them. Review your personal information security practices to improve the security of your devices and accounts.

[direct towards device suspicious / account workflow]

> You may want to be vigilant in the near future, in case the same material, or related material, re-surfaces on the internet. One way to protect yourself is to look up your name or the pseudonyms you used in search engines, to see where else they may appear. You can set up a Google Alert for yourself, or use Meltwater or Mention. These services will notify you when your name or pseudonym appears on the internet, as well.

 (consider linking to Access Now's guide on self-doxxing)

#### resources

[a series of links to learn more about the topic]
