---
layout: page
title: "Are you being targeted by online harassment?"
author: Flo Pagano, Natalia Krapiva
language: en
summary: "Are You Being Targeted by Online Harassment?"
date: 2023-04
permalink: /en/topics/harassed-online/
parent: Home
---

# Are You Being Targeted by Online Harassment?

The Internet, and social media platforms in particular, are a critical space for civil society members and organizations, especially for women, LGBTQIA+ people, and other marginalized groups, to express themselves and make their voices heard. But at the same time, they also are spaces where these groups are easily targeted for expressing their views. Online violence and abuse deny women, LGBTQIA+ persons, and many other unprivileged people the right to express themselves equally, freely, and without fear.

Online violence and abuse have many different forms, and malicious entities can often rely on impunity, also due to a lack of laws that protect victims of harassment in many countries, but most of all because protection strategies need to be tweaked creatively depending on what kind of attack is being launched.

It is therefore important to identify the typology of the attack targeting you to decide what steps can be taken.

This section of the Digital First Aid Kit will walk you through some basic steps to understand what kind of attack you are suffering and plan how to get protected against it, or find a digital security help desk that can support you.

To better understand the kind of issue you are going through, and to eventually figure out how to tackle it, it is always a good practice to gather information on what has happened in a systematic way. You can find tips on how to do this correctly in [this DFAK guide on documenting online attacks](../../../document). If you feel overwhelmed by this kind of effort, remember you don't have to do it on your own - you can ask a person you trust to support you while gathering information, or you can even give them access to your devices or accounts to do it for you. If you feel overwhelmed, you can find some advice in the [DFAK self-care guide](../../self-care).

If you are targeted by online harassment, follow this questionnaire to identify the nature of your problem and find possible solutions.

## Workflow

### physical-wellbeing

Do you fear for your physical integrity or well-being?

- [Yes](#physical-risk_end)
- [No](#location)

### location

Does the attacker appear to know your physical location?

- [Yes](#location_tips)
- [No](#device)

### location_tips

> Check your recent posts on social media: do they include your exact location? If so, disable GPS access for the social media apps and other services in your phone, so when you post your updates your location doesn't show.
>
> - [How to turn off location services on iOS](https://support.apple.com/en-us/HT207092)
> - [Manage location settings on Android](https://support.google.com/accounts/answer/3467281?hl=en)
>
>
> Check pictures you posted of yourself online: do they include details of places that are recognizable and can clearly show where you are? To protect yourself from potential stalkers, it's better not to show details of your location when posting photos or videos of yourself.
>
> As an additional precautionary measure, it is also a good idea to keep GPS off all the time – except for only briefly when you really need to find your position on a map.

Could the attacker have followed you through information you published online? Or do you still feel the attacker knows your physical location by other means?

 - [No, I think I've solved my issues](#resolved_end)
 - [Yes, I still think the attacker knows where I am](#device)
 - [No, but I have other issues](#account)

### device

Do you think the attacker has accessed or is accessing your device?

 - [Yes](#device-compromised)
 - [No](#account)


### device-compromised

> Change the password to access your device to a unique, long and complex one:
>
> - [Mac OS](https://support.apple.com/guide/mac-help/change-the-login-password-on-mac-mchlp1550/mac)
> - [Windows](https://support.microsoft.com/en-us/help/4490115/windows-change-or-reset-your-password)
> - [iOS - Apple ID](https://support.apple.com/en-us/HT201355)
> - [Android](https://support.google.com/accounts/answer/41078?co=GENIE.Platform%3DAndroid&hl=en)

Do you still have the feeling that the attacker may be controlling your device?

 - [Yes](#info_stalkerware)
 - [No, I think I've solved my issues](#resolved_end)
 - [No, but I have other issues](#account)


### info_stalkerware

> If you still have the feeling that someone may be controlling what you do on your phone, there is a possibility that your device is infected with commercial spyware, also known as stalkerware. Stalkerware apps are easy to buy and install, and they enable the abuser to secretly spy on someone else’s private life through their mobile device. These programs run hidden in the background, without the affected person knowing or giving their consent.
>
> If you think that someone may be spying on you through an app they installed on your mobile device, [this  Digital First Aid Kit workflow will help you decide whether your device is infected with malware and how to take steps to clean it](../../../device-acting-suspiciously).

Do you need more help to understand the attack you are suffering?

 - [Yes, I would like to check if my device is under control](../../../device-acting-suspiciously)
 - [No, I think I've solved my issues](#resolved_end)
 - [Yes, I still have some issues i would like to troubleshoot](#account)

### account

> Whether someone got access to your device or not, there is a possibility that they have accessed your online accounts, by hacking into them or because they knew or cracked your password.
>
> If someone has access to one or more of your online accounts, they could be reading your private messages, identify your contacts, and publish your private posts, photos, or videos, or start impersonating you.

Review your online accounts activity and your mailbox (including the Sent and Trash folders) for possible suspicious activity.

Have you noticed posts or messages disappearing, or other activities that give you good reason to think your account may have been compromised?

 - [Yes](#change-passwords)
 - [No](#private-contact)


### change-passwords

> Try changing the password to each of your online accounts with a strong and unique password.
>
> You can learn more on how to create and manage passwords in this [Surveillance Self-Defense guide](https://ssd.eff.org/module/creating-strong-passwords).
>
> It is also a very good idea to add a second-layer of protection to your online accounts by enabling 2-factor authentication whenever possible.
>
> Find out how to enable 2-factor authentication in this [Surveillance Self-Defense guide](https://ssd.eff.org/module/how-enable-two-factor-authentication).

Do you still have the feeling that someone may have access to your account?

 - [Yes](#hacked-account)
 - [No](#private-contact)


### hacked-account

If you are unable to access your account there is a chance that your account has been hacked into and the hacker has changed your password.  

If you think your account may have been hacked into, try following the Digital First Aid Kit workflow that can help you troubleshoot account access issues.

 - [Please take me to the workflow to troubleshoot account access issues](../../../account-access-issues)
 - [I think I've solved my issues](#resolved_end)
 - [My account is fine, but I have other issues](#private-contact)


### private-contact

Are you receiving unwanted phone calls on your private phone number or messages on a private messaging app?

 - [Yes](#change-contact)
 - [No](#threats)

### change-contact

> If you are receiving unwanted phone calls, SMS messages or messages on apps that are associated with your private telephone number, email, or other contact information, you can try changing your number, SIM card, email, or other contact information that is being used by the attacker.
>
> Remember to inform your contacts about this change, and consider reporting and blocking the messages and the associated account to the relevant platform.

Have you successfully stopped the unwanted calls or messages?

 - [Yes](#legal)
 - [No, I have other issues](#threats)
 - [No, I need assistance](#harassment_end)

### threats

Are you being blackmailed or receiving threats through emails or messages on a social media account?

 - [Yes](#threats-tips)
 - [No](#harassment-types)

### threats-tips

> If you are receiving messages containing threats, including threats of physical or sexual violence, or blackmail, you should first [document what happened as much as possible](../../../document), including recording any links and screenshots, and then report the person to the relevant platform or service provider and block the attacker.

Have you successfully stopped the threats?

 - [Yes](#legal)
 - [Yes, but I have more issues](#harassment-types)
 - [No, I need legal support](#legal-warning)

### harassment-types

Other forms of harassment you could be going through are impersonation, defamation, doxing, non-consensual sharing of private media, or hate speech.

What kind of situation does best describe your issue?

- [I think I'm being impersonated](#impersonation)
- [Someone is trying to damage my reputation by spreading false information](#defamation)
- [Someone has published private information or media about me without my consent](#doxing)
- [Someone is spreading hate messages against me based on attributes like race, gender, or religion](#hate-speech)
- [I think I need support to troubleshoot my situation](#harassment_end)



### impersonation

> A common form of harassment you could have been subjected to is impersonation.
>
> For example, someone may have created an account under your name and is sending private messages or publicly attacking someone, spreading disinformation or hate speech, or acting in other ways to make you, your organization, or others close to you, vulnerable to reputational and security risks.
>
> If you think someone is impersonating you or your organization, you can follow the Digital First Aid Kit workflow on impersonation, guiding you through the various forms of impersonation to identify the best possible strategy to face your issue.

What would you like to do?

 - [I am being impersonated and would like to find a solution](../../../impersonated)
 - [I think I need support to troubleshoot my situation](#harassment_end)

### defamation

> Defamation is generally defined as making a statement that injures someone's reputation. Defamation can be spoken (slander) or written (libel). Laws in different countries may differ on what constitutes defamation for the purposes of legal action. For example, some countries have laws that are very protective of freedom of expression which allow media and private individuals say things about public officials that may be embarrassing or damaging as long they believe such information is true. Other countries may allow you to more easily sue others for saying information about you that you don't like.
>
> If someone is trying to damage you or your organization's reputation, you can follow the Digital First Aid Kit workflow on defamation, which will guide you through various strategies to address defamation campaigns.

What would you like to do?

 - [I would like to find a strategy against a defamation campaign](../../../defamation)
 - [I think I need support to troubleshoot my situation](#harassment_end)

### doxing

> If someone has published private information about you or is circulating videos, photos, or other media about you, you can follow the Digital First Aid Kit workflow that can help you understand what is happening and how to respond to such attack.

What would you like to do?

 - [I'd like to understand what is happening and what I can do about it](../../../doxing)
 - [I would like to receive support](#harassment_end)

### hate-speech

> Hate speech includes written, spoken or visual discrimination, harassment, threats or violence against a person or group on the basis of their gender, disability, sexual orientation, race, etc.

Have you been attacked by one or more persons?

 - [One person](#one-person)
 - [More persons](#campaign)

### one-person

> Whether you know who your harasser is or not, it is always a good idea to block and report them on the relevant social networking platforms whenever possible. Here is a list of links to instructions on how to block users and report abuses on some of the most popular social networking platforms.
>
> - [**Facebook**](https://www.facebook.com/help/600901419958304)
> - **Twitter**
>   - [Report Abusive Behavior on Twitter](https://help.twitter.com/en/safety-and-security/report-abusive-behavior)
>   - [Block an Account on Twitter](https://help.twitter.com/en/using-twitter/blocking-and-unblocking-accounts)
> - [**Google**](https://support.google.com/accounts/answer/6388749?co=GENIE.Platform%3DDesktop&hl=en)
> - [**Instagram**](https://help.instagram.com/426700567389543)
> - **TikTok**
>   - [Blocking Users on TikTok](https://support.tiktok.com/en/using-tiktok/followers-and-following/blocking-the-users)
>   - [Reporting Abuse on TikTok](https://support.tiktok.com/en/safety-hc/report-a-problem)
> - [**Tumblr**](https://tumblr.zendesk.com/hc/en-us/articles/231877648-Blocking-users)

Have you blocked your harasser effectively?

- [Yes](#legal)
- [No](#harassment_end)


### legal

> If you know who is harassing you, you can think of reporting them to your country's authorities, if safe and appropriate in your context. Each country has different laws for protecting people from online harassment, and you should explore the legislation in your country or ask for legal advice to help you decide what to do.
>
> If you don't know who is harassing you, in some cases you could trace back the attacker's identity through a forensic analysis of the traces they may have left behind.
>
> In any case, if you are thinking of taking legal action, it will be very important to keep evidence of the attacks you were subjected to. It is therefore strongly recommended to follow the [recommendations in the Digital First Aid Kit guide on recording information on attacks](../../../documentation).

What would you like to do?

 - [I would like to receive legal help to sue my attacker](#legal_end)
 - [I think I've solved my issues](#resolved_end)


### campaign

> If you are being attacked by more than one person, you might be the target of a hate speech or harassment campaign, and you will need to reflect on what is the best strategy that applies to your case.
>
> To learn about all the possible strategies, read [Take Back The Tech's page on strategies against hate speech](https://www.takebackthetech.net/be-safe/hate-speech-strategies).

Have you identified the best strategy for you?

 - [Yes](#resolved_end)
 - [No, I need help to troubleshoot more](#harassment_end)
 - [No, I need legal assistance](#legal_end)

### harassment_end

> If you are still under attack and need a customized solution, please contact the organizations below who can support you.

:[](organisations?services=harassment)


### physical-risk_end

> If you are at physical risk, please contact the organizations below who can support you.

:[](organisations?services=physical_security)


### legal_end

> If you need legal support, please contact the organizations below who can support you.
>
> If you are thinking of taking legal action, it will be very important to keep evidence of the attacks you were subjected to. It is therefore strongly recommended to follow the [recommendations in the Digital First Aid Kit page on recording information on attacks](../../../documentation).

:[](organisations?services=legal)

### resolved_end

Hopefully this troubleshooting guide was useful. Please give feedback [via email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- **Document harassment:** It is helpful to document the attacks or any other incident you may be witnessing: take screenshots, save the messages you receive from harassers, etc. If possible, create a journal where you can systematize this documentation recording dates, times, platforms and URLs, user ID, screenshots, description of what happened, etc. Journals can help you detect possible patterns and indications about your possible attackers. If you feel overwhelmed, try to think of someone you trust who could document the incidents for you for a while. You should deeply trust the person who will manage this documentation, as you will need to hand them over credentials to your personal accounts. Before sharing your password with this person, change it to something different and share it via a secure means - such as using a tool with [end-to-end encryption](https://www.frontlinedefenders.org/en/resource-publication/guide-secure-group-chat-and-conferencing-tools). Once you feel you can regain control of the account, remember to change your password back to something unique, [secure](https://ssd.eff.org/en/module/creating-strong-passwords), and that only you know.

    - Follow the [recommendations in the Digital First Aid Kit guide on recording information on attacks](../../../documentation) to store information regarding your attack in the best possible way.

- **Set up 2-factor authentication** on all your accounts. 2-factor authentication can be very effective for stopping someone from accessing your accounts without your permission. If you can choose, don't use SMS-based 2-factor authentication and select a different option, based on a phone app or on a security key.

    - If you don't know what solution is best for you, you can check out [Access Now's "What is the best type of multi-factor authentication for me?" infographic](https://www.accessnow.org/cms/assets/uploads/2017/09/Choose-the-Best-MFA-for-you.png) and [EFF's "Guide to Common Types of Two-Factor Authentication on the Web"](https://www.eff.org/deeplinks/2017/09/guide-common-types-two-factor-authentication-web).
    - You can find instructions for setting up 2-factor authentication on the major platforms in [The 12 Days of 2FA: How to Enable Two-Factor Authentication For Your Online Accounts](https://www.eff.org/deeplinks/2016/12/12-days-2fa-how-enable-two-factor-authentication-your-online-accounts).

- **Map your online presence**. Self-doxing consists in exploring open source intelligence on oneself to prevent malicious actors from finding and using this information for impersonating you. Learn more on how to explore your online traces and prevent doxing in [this guide by Access Now Helpline](https://guides.accessnow.org/self-doxing.html).
- **Do not accept messages from unknown senders.** Some messaging platforms, like WhatsApp, Signal, or Facebook Messenger allow you to preview messages before accepting the sender as trusted. Apple iMessage also allows you to change settings in order to filter messages from unknown senders. Never accept a message or contact request if they look suspicious or you don't know the sender.


#### Resources

- [Access Now Helpline Community Documentation: Guide to prevent doxing](https://guides.accessnow.org/self-doxing.html)
- [Access Now Helpline Community Documentation: FAQ - Online Harassment Targeting a Civil Society Member](https://communitydocs.accessnow.org/234-FAQ-Online_Harassment.html)​​​​​​​
- [PEN America: Online Harassment Field Manual](https://onlineharassmentfieldmanual.pen.org/)
- [Equality Labs: Anti-doxing Guide for Activists Facing Attacks from the Alt-Right](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)
- [FemTechNet: Locking Down Your Digital Identity](https://femtechnet.org/csov/lock-down-your-digital-identity/)
- [National Network to End Domestic Violence: Documentation Tips for Survivors of Technology Abuse & Stalking](https://www.techsafety.org/documentationtips)
