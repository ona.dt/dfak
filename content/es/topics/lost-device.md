---
layout: page
title: "Perdí mi dispositivo"
author: Hassen Selmi, Bahaa Nasr, Michael Carbone, past DFAK contributors
language: es
summary: "Perdí mi dispositivo, que debo hacer?"
date: 2023-05
permalink: /es/topics/lost-device
parent: /es/
---

# Perdí mi dispositivo

¿Se ha perdido tu dispositivo? ¿Ha sido robado o incautado por un tercero?

En estas situaciones, es importante tomar medidas inmediatas para reducir el riesgo de que otra persona acceda a tus cuentas, contactos e información personal.

Esta sección del Kit de Primeros Auxilios Digitales te guiará a través de algunas preguntas básicas para que puedas evaluar cómo reducir los posibles daños relacionados con la pérdida de un dispositivo.

## Workflow

### question-1

¿Todavía está perdido el dispositivo?

  - [Sí, está perdido](#device-missing)
  - [No, me lo han devuelto](#device-returned)

### device-missing

> Es bueno reflexionar sobre qué medidas de seguridad tenía el dispositivo:
>
> - ¿El acceso al dispositivo está protegido por una contraseña, pin, patrón u otra medida de seguridad?
> - ¿El dispositivo tiene activado el cifrado general del almacenamiento?
> - ¿En qué estado se encontraba tu dispositivo cuando se perdió? ¿Estaba conectado? ¿Estaba encendido pero con contraseña? ¿Estaba suspendido o hibernando? ¿Completamente apagado?

Teniendo esto en cuenta, puedes comprender mejor la probabilidad de que otra persona tenga o haya tenido acceso al contenido de tu dispositivo.

- [Eliminemos el acceso del dispositivo a mis cuentas](#accounts)

### accounts

> Enumera todas las cuentas a las que el dispositivo en cuestión tuvo acceso. Estas pueden ser cuentas de correo electrónico, redes sociales, mensajería, aplicaciones de citas y servicios bancarios, así como cuentas que pueden usar este dispositivo como segundo factor de autenticación.
>
> Para cualquier cuenta a la que tu dispositivo haya tenido acceso, debes eliminar la autorización de este dispositivo para el acceso a ellas. Esto se puede hacer iniciando sesión en tus cuentas y eliminando el dispositivo de la lista de dispositivos permitidos.
>
> - [Cuenta de Facebook](https://www.facebook.com/settings?tab=security&section=sessions&view)
> - [Cuenta de Google](https://myaccount.google.com/device-activity)
> - [Cuenta de iCloud](https://support.apple.com/es-us/HT205064)
> - [Cuenta de Twitter](https://twitter.com/settings/applications)
> - [Cuenta de Yahoo](https://login.yahoo.com/account/activity)

Una vez que hayas completado la desvinculación de tus cuentas, vamos a asegurar las contraseñas que pueden haber estado en el dispositivo.

- [Bien, vamos a trabajar con las contraseñas](#passwords)

### passwords

> Piensa en las contraseñas guardadas directamente en el dispositivo o en cualquier navegador en donde hayas almacenado contraseñas.
>
> Cambia las contraseñas para todas las cuentas a las que este dispositivo pueda acceder. Si no usas un administrador de contraseñas, considera usar uno para crear y administrar contraseñas seguras.

Después de cambiar las contraseñas para las cuentas en tu teléfono, piensa en si usaste alguna de estas contraseñas para otras cuentas. Si es así, cambia también esas contraseñas.

- [Utilicé algunas de mis contraseñas en el dispositivo perdido para otras cuentas](#same-password)
- [Todas mis contraseñas que pudieron haber sido comprometidas fueron únicas](#2fa)

### same-password

¿Utilizas la misma contraseña en otras cuentas o dispositivos además del dispositivo perdido? Si es así, cambia la contraseña en esas cuentas, ya que también pueden haberse visto comprometidas.

- [bien, ya he cambiado todas las contraseñas relevantes](#2fa)

### 2fa

> La activación de la autenticación de dos factores (2fa) en las cuentas que crees que pueden estar en riesgo de ser accedidas reducirá la posibilidad de que otra persona pueda acceder a ellas.
>
> Activa la autenticación de dos factores para todas las cuentas a las que este dispositivo pueda acceder. Ten en cuenta que no todas las cuentas admiten la autenticación de dos factores. Para obtener una lista de servicios y plataformas que admiten la autenticación de dos factores, visita [Two Factor Auth](https://twofactorauth.org).

- [He habilitado la autenticación de dos factores en mis cuentas para protegerlas aún más. Me gustaría intentar encontrar o borrar mi dispositivo](#find-erase-device)

### find-erase-device

> Piensa para qué utilizaste este dispositivo: ¿hay información confidencial en este dispositivo, como contactos, ubicación o mensajes? ¿Pueden estos datos ser problemáticos para ti, tu trabajo u otros si se filtran?
>
> En algunos casos, puede ser útil borrar de forma remota los datos en el dispositivo de una persona detenida, para evitar que se abuse de ellos y se usen contra ellos u otros activistas. Al mismo tiempo, esto puede ser problemático para una persona detenida (especialmente en situaciones donde es posible la tortura y el maltrato). Si la persona detenida fue obligada a dar acceso al dispositivo, el hecho de que los datos desaparezcan repentinamente del dispositivo puede generar sospechas en las autoridades. Lee nuestra sección ["Alguien que conozco ha sido arrestado"](../../../../arrested) para más detalles.
>
> También vale la pena señalar que en algunos países la eliminación remota puede ser contraproducente a nivel legal, ya que podría interpretarse como destrucción de evidencia.
>
> Si tienes claras las consecuencias directas y legales para todas las personas involucradas, puedes continuar con tus intentos de limpiar el dispositivo de forma remota, siguiendo las instrucciones de su sistema operativo:
>
> - [Dispositivos Android](https://support.google.com/accounts/answer/6160491?hl=es)
> - [iOS o MacOS](https://www.icloud.com/#find)
> - [Dispositivos iOS (iPhone y iPad) usando iCloud](https://support.apple.com/kb/PH2701?locale=en_US&viewlocale=es_ES)
> - Para dispositivos Windows o GNU/Linux, es posible que se haya instalado algún programa (como *software* antirrobo o antivirus) que te permita borrar de forma remota los datos y el historial del dispositivo. Si es así, úsalo.

Independientemente de que hayas conseguido borrar remotamente la información del dispositivo, es recomendable informar a tus contactos de lo sucedido.

- [Continúa con los siguientes pasos para encontrar consejos sobre cómo informar a tus contactos sobre la pérdida de tu dispositivo.](#inform-network)

### inform-network

> Además de tus propias cuentas, es probable que tu dispositivo tenga información sobre terceros. Esto puede incluir tus contactos, comunicaciones con otros y grupos de mensajería.
>
> Al considerar informar a tu red y a la comunidad sobre el dispositivo perdido, usa los [principios de reducción de daños](../../../../arrested#harm-reduction) para asegurarte de que al comunicarte con otras personas no las estés poniendo en riesgo.

Informa a tu red sobre la pérdida del dispositivo. Esto se puede hacer de manera privada con contactos clave de alto riesgo, o publicando una lista de cuentas potencialmente comprometidas públicamente en tu sitio web o en una cuenta de redes sociales si consideras que es apropiado hacerlo.

- [He informado a mis contactos](#review-history)

### review-history

> Si es posible, revisa el historial de conexión/actividad de todas las cuentas conectadas al dispositivo en cuestión. Verifica si tu cuenta fue usada en un momento en el que no estabas en línea o si se accedió a tu cuenta desde una ubicación o dirección IP desconocida.
>
> Puede revisar tu actividad en algunos proveedores populares a continuación:
>
> - [Cuenta de Facebook](https://www.facebook.com/settings?tab=security&section=sessions&view)
> - [Cuenta de Google](https://myaccount.google.com/device-activity)
> - [Cuenta de iCloud](https://support.apple.com/es-lamr/HT205064)
> - [Cuenta de Twitter](https://twitter.com/settings/sessions)
> - [Cuenta de Yahoo](https://login.yahoo.com/account/activity)

¿Has revisado tu historial de conexiones?

- [Sí, y no encontré nada sospechoso](#check-settings)
- [Sí, y encontré actividad sospechosa](../../../account-access-issues)

### check-settings

> Verifica la configuración de todas las cuentas conectadas al dispositivo. ¿Han sido cambiados? Para las cuentas de correo electrónico, verifica los reenvíos automáticos, los posibles cambios en la copia de seguridad, restablece la dirección de correo electrónico y/o los números de teléfono, la sincronización con otros dispositivos, incluidos los teléfonos, computadoras o tabletas, y los permisos para las aplicaciones u otros permisos de cuenta.
>
> Repite la revisión del historial de actividad de la cuenta al menos una vez por semana durante un mes, para asegurarse de que tu cuenta no muestre actividad sospechosa.

- [El historial de actividad de mi cuenta muestra actividad sospechosa](../../../account-access-issues)
- [El historial de actividad de mi cuenta no muestra ninguna actividad sospechosa, sin embargo, continuaré revisándolo con el tiempo](#resolved_end)


### device-returned

> Si perdiste tu dispositivo, lo tomó un tercero o tuviste que entregarlo en un cruce fronterizo, por ejemplo, pero lo has recuperado, ten cuidado ya que no sabes quién ha tenido acceso a él. Dependiendo del nivel de riesgo al que te enfrentes, es posible que desees tratar el dispositivo como si estuviera comprometido.
>
> Hazte las siguientes preguntas y evalúa el riesgo de que tu dispositivo se haya visto comprometido:
>
> - ¿Cuánto tiempo estuvo el dispositivo fuera de tu vista?
> - ¿Quién podría haber tenido acceso a él?
> - ¿Por qué querrían tener acceso a él?
> - ¿Hay signos de que el dispositivo ha sido manipulado físicamente?
>
> Si ya no confías en tu dispositivo, considera la posibilidad de limpiar y reinstalar el dispositivo u obtener uno nuevo.

¿Te gustaría recibir ayuda para obtener un dispositivo de reemplazo?

- [Sí](#new-device_end)
- [No](#resolved_end)


### accounts_end

Si has perdido el acceso a tus cuentas o crees que alguien podría haber accedido a ellas, comunícate con las organizaciones listadas a continuación que pueden ayudarte.

:[](organisations?services=account)

### new-device_end

Si necesitas ayuda financiera para obtener un dispositivo de reemplazo, comunícate con las organizaciones listadas a continuación que pueden ayudarte.

:[](organisations?services=equipment_replacement)

### resolved_end

Esperamos que esta guía del Kit de Primeros Auxilios Digitales te haya sido útil. Por favor danos tu opinión [vía correo electrónico](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com).

### final_tips

A continuación verás una serie de consejos para mitigar el riesgo de fuga de datos y acceso no autorizado a tus cuentas e información:

- Nunca dejes tu dispositivo desatendido. Si necesitas hacerlo, apágalo.
- Habilita el cifrado de disco completo.
- Usa una contraseña segura para bloquear tu dispositivo.
- Activa la función Buscar/Borrar mi teléfono siempre que sea posible, pero ten en cuenta que esto podría usarse para rastrear o eliminar tu dispositivo, si tu cuenta asociada (Gmail/iCloud) está comprometida.

#### Resources

- [Documentación para la comunidad de la línea de ayuda en seguridad digital de Access Now: Consejos sobre cómo habilitar el cifrado de disco completo - (en inglés)](https://communitydocs.accessnow.org/166-Full-Disk_Encryption.html).
- [Security in a Box: Tácticas para proteger archivos confidenciales (en inglés)](https://securityinabox.org/es/files/secure-file-storage/).
- Considera utilizar software antirrobo como [Prey](https://preyproject.com/es/).
