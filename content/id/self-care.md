---
layout: sidebar.pug
title: "Feeling Overwhelmed?"
author: FP
language: id
summary: "Pelecehan online, ancaman, dan jenis serangan digital lainnya dapat menciptakan perasaan kewalahan dan kondisi emosional yang sangat rapuh: Anda mungkin merasa bersalah, malu, cemas, marah, bingung, tidak berdaya, atau bahkan takut akan kesehatan psikologis atau fisik Anda."
date: 2018-09-05
permalink: /id/self-care/
parent: Home
sidebar: >
  <h3>Baca lebih lanjut mengenai cara melindungi diri sendiri dari perasaan kewalahan:</h3>

  <ul>
    <li><a href="https://www.newtactics.org/conversation/self-care-activists-sustaining-your-most-valuable-resource">Self-Care untuk Aktivis: Mempertahankan Sumber Daya Anda yang Paling Berharga</a></li>
    <li><a href="https://www.amnesty.org.au/activism-self-care/">Merawat diri Anda sendiri agar Anda dapat terus membela hak asasi manusia</a></li>
    <li><a href="https://iheartmob.org/resources/self_care">Self-Care untuk Anda yang Mengalami Kekerasan</a></li>
    <li><a href="https://www.fightcyberstalking.org/emotional-support">Dukungan Emosional untuk Korban Cyberstalking</a></li>
    <li><a href="https://cyber-women.com/en/self-care/">Modul pelatihan self-care untuk perempuan di dunia maya</a></li>
    <li><a href="https://onlineharassmentfieldmanual.pen.org/self-care/">Kesehatan dan Komunitas</a></li>
    <li><a href="https://www.patreon.com/posts/12240673">Dua puluh cara untuk membantu seseorang yang mengalami perundungan online</a></li>
    <li><a href="https://www.hrresilience.org/">Proyek Human Rights Resilience</a></li>
  </ul>
---

# Merasa Kewalahan?

Pelecehan online, ancaman, dan jenis serangan digital lainnya dapat menciptakan perasaan kewalahan dan kondisi emosional yang sangat rapuh: Anda mungkin merasa bersalah, malu, cemas, marah, bingung, tidak berdaya, atau bahkan takut akan kesehatan psikologis atau fisik Anda.

Tidak ada cara yang "benar" untuk merasakan sesuatu, karena kondisi kerentanan dan arti informasi pribadi bagi seseorang berbeda dari yang satu dengan yang lainnya. Emosi apa pun tidak ada yang salah dan Anda tidak perlu khawatir apakah reaksi Anda benar atau tidak.

Hal pertama yang harus Anda ingat adalah bahwa apa yang terjadi pada Anda bukanlah kesalahan Anda dan Anda tidak seharusnya menyalahkan diri sendiri, tetapi jika memungkinkan hubungilah seseorang yang dipercaya yang dapat mendukung Anda dalam mengatasi keadaan darurat ini.

Untuk meminimalisir serangan online, Anda perlu mengumpulkan informasi mengenai apa yang telah terjadi, tapi Anda tidak harus melakukannya sendirian - jika Anda memiliki orang yang Anda percaya, Anda dapat meminta mereka untuk membantu Anda sembari mengikuti instruksi di situs web ini, atau beri mereka akses ke perangkat atau akun Anda untuk mengumpulkan informasi untuk Anda.
