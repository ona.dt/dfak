---
layout: page.pug
title: "About"
language: my
summary: "Digital First Aid Kit(ဒစ်ဂျစ်တယ်ရှေးဦးသူနာပြုလမ်းညွှန်) အကြောင်း"
date: 2020-11
permalink: /my/about/
parent: Home
---

ဒစ်ဂျစ်တယ်ရှေးဦးသူနာပြုလမ်းညွှန်သည် [RaReNet (Rapid Response Network)](https://www.rarenet.org/) နှင့် [CiviCERT](https://www.civicert.org/) တို့ ပူးပေါင်းဆောင်ရွက်ထားပါသည်။

<iframe src="https://archive.org/embed/dfak-tech-demo" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

Rapid Response Network ဆိုသည်မှာ လျင်မြန်စွာတုံ့ပြန်သူများ နှင့် ဒစ်ဂျစ်တယ်လုံခြုံရေးချန်ပီယံများဖြစ်သည့် Access Now, Amnesty Tech, Center for Digital Resilience, CIRCL, EFF, Freedom House, Front Line Defenders, Global Voices, Greenhost, Hivos & the Digital Defenders Partnership, Internews, La Labomedia, Open Technology Fund, Virtualroad အပြင် ထိုနယ်တွင်လုပ်ဆောင်နေကြသော ကျွမ်းကျင်သူများအားလုံးအတူလုပ်ကိုင်နေသော အပြည်ပြည်ဆိုင်ရာကွန်ယက်တစ်ခုဖြစ်ပါသည်။

အဖွဲ့အစည်းများနှင့် CiviCERT ၏အစိတ်အပိုင်းဖြစ်သော တစ်ဦးတစ်ယောက်ချင်းစီပါဝင်သော အပြည်ပြည်ဆိုင်ရာကွန်ယက်တစ်ခုဖြစ်ပြီး ဒစ်ဂျစ်တယ်လုံခြုံရေးအကူအညီပေးနေသည့်နေရာများနှင့် အခြေခံဝန်ဆောင်မှုပေးသူများ၏ လူမှုကွန်ယက်တရားမျှတမှုနှင့် လူ့အခွင့်အရေးနှင့်ဒစ်ဂျစ်တယ်အခွင့်အရေးများကိုကာကွယ်ရန်ကြိုးပမ်းနေသည့်အဖွဲ့များနှင့် အဖွဲ့အစည်းများကို အဓိကအာရုံစိုက်သည်။ CiviCERT သည် လျင်မြန်စွာတုန့်ပြန်အသိုင်းအဝိုင်းရဲ့ဖြန့်ဝေသည့် CERT (ကွန်ပျူတာအရေးပေါ်တုံ့ပြန်မှုအဖွဲ့) အားထုတ်မှုတစ်ခုဖြစ်ပါသည်။ CiviCERT သည်ယုံကြည်စိတ်ချရသောကွန်ပျူတာအရေးပေါ်တုံ့ပြန်မှုအဖွဲ့များ၏ဥရောပကွန်ယက် မှအသိအမှတ်ပြုထားသည်။

ဒစ်ဂျစ်တယ်ရှေးဦးသူနာပြုလမ်းညွှန်သည် [ပြင်ပပံ့ပိုးမှုများကိုလက်ခံသော open-source စီမံကိန်း](https://gitlab.com/rarenet/dfak) တစ်ခုဖြစ်သည်။

အကယ်၍ သင်ဆက်သွယ်မှုအကန့်အသတ်ရှိသည့်အခြေအနေမျိုးတွင် ဒစ်ဂျစ်တယ်ရှေးဦးသူနာပြုလမ်းညွှန်ကိုအသုံးပြုလိုပါက သို့မဟုတ် ဆက်သွယ်မှုတစ်ခုခက်ခဲခြင်းကိုအသုံးပြုလိုပါက ဒေါင်းလော့ဆွဲနိုင်သည်။ [an offline version](https://digitalfirstaid.org/dfak-offline.zip)

ဒစ်ဂျစ်တယ်ရှေးဦးသူနာပြုလမ်းညွှန်ကို မှတ်ချက်ပေးခြင်း၊ အကြုံပြုချက်ပေးခြင်း သို့မဟုတ် မေးစရာမေးခွန်းများ ရှိပါက dfak @ digitaldefenders . org သို့ အီးမေးလ်ပေးပို့နိုင်ပါသည်။

GPG - Fingerprint: 1759 8496 25C1 56EC 1EB4 1F06 6CC1 888F 5D75 706B
