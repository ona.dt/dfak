---
name: Digital Security Lab Ukraine
website: https://dslua.org/
logo: DSL-Ukraine.png
languages: English, Українська, Русский, Français
services: in_person_training, web_protection, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, forensic, legal, individual_care, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: Segunda-Quinta 9h-17h EET/EEST
response_time: 2 dias
contact_methods: email, pgp, signal, whatsapp
email: koushnir@gmail.com, gudvadym@gmail.com
pgp: koushnir@gmail.com - 000F08EA02CE0C81, gudvadym@gmail.com - B20B8DA28B2FA0C3
signal: +380987767783; +380990673853
whatsapp: +380987767783; +380990673853
---

Digital Security Lab é uma organização não-governamental baseada em Kiev, Ucrânia, fundada em 2017 por quatro pessoas iniciadas pelo treinamento de treinadores do [ISC Project](https://www.iscproject.org/). Nossa missão é apoiar a profusão dos direitos humanos na internet, construindo capacidades junto a ONGs e mídias independentes para que resolvam suas preocupações com relação à segurança digital, e impactando políticas governamentais e corporativas no campo dos direitos digitais.
